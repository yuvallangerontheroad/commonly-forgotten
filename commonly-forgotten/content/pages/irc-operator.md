title: IRC tasks.

## Forward to another channel.

Let's say you have an existing channel named `#channel` and you want to move to `#newchannel`.

Use these commands on the libera.chat IRC network (may also work on other networks. I do not know) to forward users who join `#channel` to `#newchannel`:

```
/msg chanserv set #yourchannel mlock +if #yourotherchannel
/msg chanserv set #yourchannel guard on
```

NOTICE: You must not set any akick, or the kicked users would not be forwarded to your new channel.
