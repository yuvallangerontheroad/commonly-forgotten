title: pixel manipulation in Godot-engine.

# Per pixel manipulation in Godotengine.

* [Image class](https://docs.godotengine.org/en/stable/classes/class_image.html)
* [Can I create an image and draw to it in Godot?](https://godotengine.org/qa/78068/can-i-create-an-image-and-draw-to-it-in-godot?show=78068#q78068)
    * [this is the minimal code that you need to draw pixels to screen using an image](https://gist.github.com/kasthor/da23d1ed802a1eab7ac354305608f40f)
