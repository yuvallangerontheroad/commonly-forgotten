title: Notes on terminal programming.

# Notes on terminal programming.

If you want get keys pressed immediately, not only after pressing enter, amongst other nice terminal programs features, you have to talk with the "termios driver", according to this talk, at 09:06.

- `stty -onlcr` - will disable the "translate newline to carriage return newline" (from man's STTY(1)), so its negation is just move cursor one row down, without changing column.
- `stty -echo` - disable STTY(1) "echo input characters" or the "Where did my keyboard go?!" mode.
- `stty -icanon` - disable the "line editor" (Brandon Rhodes).
        STTY(1): "enable special characters: erase, kill, werase, rprnt"
- `stty raw` - too?

`Animations In The TERMINAL [Animating With ASCII]`: <https://youtu.be/K_6peGEsq0U?t=9m6s>

You can read stty state with `stty -g` at the start of the program and then use that as the argument of stty to return to the previous state.
While using it to read the state within a program, stty complained about something like "not a typewriter". Need to figure that one out. Right now there is no other way but to use something like termion, the "gay space communism" (sheesh...) project ([I wish I were kidding...](https://gitlab.redox-os.org/redox-os/termion/-/blob/master/examples/commie.rs)).

Oops! According to `#rust:matrix.org` I can do this:

```
std::process::Command::new("stty")
    .arg("-g")
    .stdin(std::process::Stdio::inherit())
    .output()
```

What exactly is inherited stdin? the same stdin as the father process?
